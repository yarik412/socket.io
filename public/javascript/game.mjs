import {
  createRoom, addPlayer, changeCircleReady, startGameTimer, startGameClient, changeLine, changeLineColor,
  showWinners, clearLineCircleNotReady, chngUserCounter, hideShowRooms
  } from "./helpers/listeners.js";
import { userInfo } from './game/player.js';
import { deleteElem } from './helpers/deleteElem.js';

const username = sessionStorage.getItem("username");
const roomNameDOM = document.getElementById("room-name");

if (!username) {
  window.location.replace("/login");
}
const userId = Date.now();
userInfo.username = username;
userInfo.userId = userId;


const socket = io("", { query: { username } });

socket.on("Add new room",({roomId,roomName}) => createRoom(roomId,roomName));

socket.on("Create players",(players) => addPlayer(players));

socket.on("Delete player",(id) => deleteElem(id));

socket.on('Add roomName to game', (name)=>roomNameDOM.innerHTML=name);

socket.on('Plus user',(roomId)=>chngUserCounter(roomId,"decrement"));

socket.on('Minus user',(roomId)=> chngUserCounter(roomId,"increment"));

socket.on('change ready',(idPlayer)=>changeCircleReady('not-ready','ready',idPlayer));

socket.on('change notReady',(idPlayer)=>changeCircleReady('ready','not-ready',idPlayer));

socket.on('Start timer',(time)=>startGameTimer(time));

socket.on('Start games',(data)=>startGameClient(data.text,data.time));

socket.on('Change player line',(data)=>changeLine(data.id,data.percent));

socket.on('Set light-green for line',(userId)=>changeLineColor(userId));

socket.on('Winners',(data)=>showWinners(data));

socket.on('Clear lines',(clients)=>clearLineCircleNotReady(clients));

socket.on('Hide room',(roomId)=>hideShowRooms(roomId,"hide"));

socket.on('Show room',(roomId)=>hideShowRooms(roomId,"show"))

socket.on("Error",(msg)=>alert(msg));

export function sendRoom(id,name){
  socket.emit("Create Room",{roomId:`${id}`,roomName:name, counter:0});
}
export function joinToRoomSocket(id){
  socket.emit("Join to Room", id,userInfo);
}
export function leaveFromRoom(id){
  socket.emit("Leave from room",id,userInfo);
}
export function readySocket(id){
  socket.emit("Player Ready",id,userInfo.roomId);
}
export function notReadySocket(id){
  socket.emit("Player NotReady",id,userInfo.roomId);
}
export function startGame(){
  socket.emit("Start Game",userInfo.roomId);
}
export function changeLineSocket(percent){
  socket.emit("Change Line", userInfo.roomId,userInfo.userId,percent);
}
export function sendResultSocket(roomId,userId){
  socket.emit("Player finish",roomId,userId);
}
export function endGameSocket(percent){
  socket.emit("Game end",userInfo.roomId, userInfo.userId, percent);
}
export function closeWindow(){
  socket.emit("Close Window",userInfo.roomId, userInfo.userId, username);
}
