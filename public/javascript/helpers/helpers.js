export function timer(time,element,func){
    tick();
    function tick(){
        if(time!==0){
            element.innerHTML = time;
            time--;
            setTimeout(tick, 1000);
        }else{
            func();
        }
    };
}

export function ascii (a) { return a.charCodeAt(0); }