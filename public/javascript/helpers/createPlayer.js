import { createElement } from '../helpers/domHelper.js';
import { userInfo } from '../game/player.js';

export function createPlayer({username,userId}){
    if(userId===userInfo.userId){
        username=`${username} (you)`;
    }
    const div = createElement({
        tagName:'div',
        className:'user-status',
        attributes:{
            id:userId
        }
    });
    const title = createTitle(username,userId);
    const line = createLine(userId);
    div.append(title);
    div.append(line);

    return div;
}

function createTitle(username,id){
    const title = createDiv('user-status__title flex flex-align-centered')
    const circle = createElement({
        tagName:'div',
        className:'circle not-ready',
        attributes:{
            id:`${id}-circle`
        }
    });
    const name = createDiv('left_pad-mid');
    name.innerHTML = username;
    title.append(circle);
    title.append(name);

    return title;
}

function createLine(id){
    const div = createDiv('user-status__line marg-top_small');
    const line = createElement({
        tagName:'div',
        className:'color-line size-line',
        attributes:{
            id:`${id}-line`
        }
    });
    div.append(line);

    return div;
}

function createDiv(className){
    return createElement({
        tagName:'div',
        className:className
    })
}