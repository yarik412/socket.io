export function deleteElem(id){
    const elem = document.getElementById(id);
    elem.remove();
}
export function clearAllChild(elem){
    while (elem.firstChild) {
        elem.removeChild(elem.lastChild);
    }
}