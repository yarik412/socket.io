import { 
    sendRoom, joinToRoomSocket, leaveFromRoom, readySocket, notReadySocket,
    startGame, changeLineSocket, sendResultSocket, endGameSocket, closeWindow
    } from "../game.mjs";
import { userInfo } from '../game/player.js';
import { createRoomCard } from "./createRoomElem.js";
import { createPlayer } from "./createPlayer.js";
import { timer, ascii } from './helpers.js';
import { clearAllChild } from './deleteElem.js';

const listRooms = document.getElementById("list-rooms");
const modal = document.getElementById("modal");
const rooms = document.getElementById("rooms-page");
const game = document.getElementById("game-page");
const playersList = document.getElementById("users-list");
const textArea = document.getElementById("game-text");

const pressed = new Set();
let text;
let keys;
let counter = 0;


export function createNewRoom(){
    const id = Date.now();
    let name = document.getElementById("name-room").value;
    sendRoom(id,name);
    hideModal();
};
export function createRoom(id,name){
    const users = createRoomCard("0",name,`${id}`);
    listRooms.append(users);
}

export function showModal(){
    showElement(modal);
}

export function joinToRoom(id){
    joinToRoomSocket(id);
    userInfo.roomId = id;
    hideElement(rooms);
    showElement(game);
    windowClose();
}
export function backToRooms(){
    leaveFromRoom(userInfo.roomId);
    clearAllChild(playersList);
    hideElement(game);
    showElement(rooms);
}
export function addPlayer(players){
    players.map(player=>{
        const user = createPlayer(player);
        playersList.append(user);
    });
}
export function readyPlayer(id=userInfo.userId){
    const ready = document.getElementById("readyButton");
    const notReady = document.getElementById("notReadyButton");
    hideElement(ready);
    showElement(notReady);
    readySocket(id);
}
export function notReadyPlayer(id=userInfo.userId){
    const ready = document.getElementById("readyButton");
    const notReady = document.getElementById("notReadyButton");
    hideElement(notReady);
    showElement(ready);
    notReadySocket(id);
}
export function changeCircleReady(removeClass,addClass,id=userInfo.userId){
    const circle = document.getElementById(`${id}-circle`);
    circle.classList.remove(removeClass);
    circle.classList.add(addClass);
}
export function startGameTimer(time){
    const notReady = document.getElementById("notReadyButton");
    const timerDOM = document.getElementById("game-startTimer");
    const backButton = document.getElementById("backToRooms");
    hideElement(backButton);
    showElement(timerDOM);
    hideElement(notReady);
    timer(time,timerDOM,startGame);
}
export function startGameClient(text,time){
    const startTimerDOM = document.getElementById("game-startTimer");
    const gameTimerDiv = document.getElementById("game-timer");
    const gameTimer = document.getElementById("game-timerTime");
    hideElement(startTimerDOM);
    textArea.innerHTML = text;
    showElement(gameTimerDiv);
    timer(time, gameTimer, callEndFunc);
    addListener(textArea);
}
export function showWinners(data){
    const winnersModal = document.getElementById("modal-winer");
    const winnerDIV = document.getElementById("winer-list");
    const gameTimerDiv = document.getElementById("game-timer");
    showElement(winnersModal);
    hideElement(gameTimerDiv);
    textArea.innerHTML = ''
    data = data.map((elem,index)=>`${index+1}.${elem}`)
    winnerDIV.innerHTML = data.join('<br>');
}
export function hideWinnerModal(){
    const winnersModal = document.getElementById("modal-winer");
    const ready = document.getElementById("readyButton");
    const backButton = document.getElementById("backToRooms");
    endGame();
    hideElement(winnersModal);
    showElement(backButton);
    showElement(ready);
}

export function changeLine(id,percent){
    const line = document.getElementById(`${id}-line`);
    line.style.width = `${percent}%`;
}

export function changeLineColor(id){
    const line = document.getElementById(`${id}-line`);
    line.style.backgroundColor = 'var(--ready-user-background)';
}

export function clearLineCircleNotReady(arrObjs){
    arrObjs.forEach((user)=>{
        const id = user.userId;
        changeLine(id, 0);
        changeCircleReady("ready","not-ready",id);
    });
}

export function chngUserCounter(roomId,type){
    const elem = document.getElementById(`${roomId}-usersRoom`)
    let text = elem.innerHTML.split(' ');
    if(type === 'decrement'){
        text[0] = Number(text[0]) + 1;
    }else{
        text[0] = Number(text[0]) - 1;
    }
    elem.innerHTML = text.join(' ');
}

export function hideShowRooms(roomId,type){
    const elem = document.getElementById(`${roomId}-card`);
    if(type==='hide'){
        elem.classList.add('display-none');
    }else{
        elem.classList.remove('display-none');
    }
}


function addListener(textArea){
    text = textArea.innerText.split('');
    keys = text.map((item)=> ascii(item));
    text = text.map((el) => el===' ' ? '&nbsp;' : el);

    document.addEventListener('keypress', checkTyping);
}

function checkTyping(e) {
    pressed.add(e.charCode);
    if(pressed.has(keys[counter])){
        text[counter] = '<span class="checked-letter">'+text[counter]+'</span>';
        if(text[counter+1]){
            text[counter+1] = '<span class="undeline-letter">'+text[counter+1]+'</span>';
        }
        textArea.innerHTML = text.join('');
        counter += 1;
        const percent = counter/keys.length*100;
        changeLineSocket(percent);
        percent === 100 ? sendResult(userInfo.roomId, userInfo.userId) : null;
    }
    pressed.delete(e.charCode);
}

function callEndFunc(){
    const percent = counter/keys.length*100;
    endGameSocket(percent);
}

function endGame(){
    document.removeEventListener('keypress', checkTyping);
    const gameTimerDiv = document.getElementById("game-timer");
    hideElement(gameTimerDiv);
    textArea.innerHTML = '';
    pressed.clear();
    counter = 0;
}


function sendResult(roomId,userId){
    sendResultSocket(roomId,userId);
}

function hideModal(){
    hideElement(modal);
    document.getElementById("name-room").value = '';
}

function hideElement(element){
    element.classList.add("display-none");
}
function showElement(element){
    element.classList.remove("display-none");
}
function windowClose(){
    window.addEventListener('beforeunload', (event) => {
        event.preventDefault();
        event.returnValue = '';
        closeWindow();
    });
}

window.createNewRoom = createNewRoom;
window.showModal = showModal;
window.hideModal = hideModal;
window.joinToRoom = joinToRoom;
window.backToRooms = backToRooms;
window.readyPlayer = readyPlayer;
window.notReadyPlayer = notReadyPlayer;
window.hideWinnerModal = hideWinnerModal;