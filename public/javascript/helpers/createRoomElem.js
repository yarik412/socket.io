import { createElement } from '../helpers/domHelper.js';

export function createRoomCard(countUsers,roomName,id){
  const card = createElement({
    tagName:'div',
    className:'room-card',
    attributes:{
      id:`${id}-card`
    }
  })
  const users = createNumberUsers(countUsers,id);
  const content = createRoomTitle(roomName,id);
  card.append(users);
  card.append(content);
  return card;
}
function createNumberUsers(countUsers,id){
  const div = createElement({
    tagName:'div',
    className:'room-users flex flex-align-centered left_pad-mid'
  });
  const p = createElement({
    tagName:'a',
    attributes:{
      id:`${id}-usersRoom`
    }
  });
  p.innerHTML=`${countUsers} user connected`;
  div.append(p);
  return div;
}

function createRoomTitle(roomName,id){
  const div = createElement({
    tagName:'div',
    className:'room-card__content flex room-card__content_flex'
  });
  const title = createElement({
    tagName:'h2',
    className:'room-title_font'
  });
  const button = createElement({
    tagName:'button',
    className:'rooms-button_small',
    attributes:{
      id:id,
      onclick:"joinToRoom(this.id)"
    }
  });
  title.innerHTML=roomName;
  button.innerHTML="Join";
  div.append(title);
  div.append(button);
  return div;
}