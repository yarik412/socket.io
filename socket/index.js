import * as config from "./config";
import texts from "../data.js";

const clients = new Map();
const roomsKeyName = new Map();
const roomsKeyId = new Map();
const results = new Map();

function addUserToMap(id,userData){
  if(clients.has(id)){
    const allClients = clients.get(id);
    allClients.push(userData);
  }else{
    clients.set(id,[userData]);
  }
}
function removeUserInMap(id,username){
  const clientsList = clients.get(id);
  if(clientsList){
    const newClientsList = clientsList.filter(user => user.username!=username);
    clients.set(id,newClientsList);
  }
}

function addNewRoom({roomName,roomId,counter},map){
  if(roomsKeyName.has(roomName)){
    return false;
  }else{
    roomsKeyName.set(roomName,{roomId,counter});
    roomsKeyId.set(roomId,{roomName,counter});
    return true;
  }
}
function decremCount(id){
  roomsKeyId.get(id).counter+=1;
}
function incremCount(id){
  roomsKeyId.get(id).counter-=1;
}

function removeRoomInMap({roomName,roomId}){
  const roomsNameList = roomsKeyName.get(roomName);
  const newRoomNameList = roomsNameList.filter(room => room.roomId!=roomId);
  const roomsIdList = roomsKeyId.get(roomId);
  const newRoomIdList = roomsIdList.filter(room => room.roomName!=roomName);
  roomsKeyName.set(roomName,newRoomNameList);
  roomsKeyId.set(roomId,newRoomIdList);
}
function setResult(roomId,userId,percent){
  if(percent){
    if(results.has(roomId)){
      const allResults = results.get(roomId);
      allResults.push({userId, percent});
    }else{
      results.set(roomId,[{userId, percent}]);
    }
  };
}
function deleteDuplicates(data){
  data = [...new Map(data.map(item => [item.userId, item])).values()];
  return data;
}
function compare(a,b){
  if (a.percent < b.percent) {
    return 1;
  } else if (a.percent > b.percent) {
    return -1;
  }
}

function sortWinners(data){
  return data.sort(compare);
}

function getWinnersName(roomId,data){
  const winners = [];
  const playerInRoom = clients.get(roomId);
  data.forEach((playerResult)=>{
    playerInRoom.forEach((player)=>{
      playerResult.userId === player.userId ? winners.push(player.username) : null;
    })
  });
  return winners
}

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    console.log(username);

    socket.on('disconnect',()=>{
      console.log(`${username} disconected.`);
    });

    socket.on('Create Room',(roomData)=>{
      if(addNewRoom(roomData)){
        socket.emit('Add new room',roomData);
        socket.broadcast.emit('Add new room',roomData);
      }else{
        socket.emit('Error',"This name for room can't be use!");
      }
    });

    socket.on('Join to Room',(roomId,userData)=>{
      if(!clients.get(roomId) || clients.get(roomId).length < config.MAXIMUM_USERS_FOR_ONE_ROOM){
        addUserToMap(roomId,userData);

        const roomClients = clients.get(roomId);
        socket.join(roomId);

        socket.broadcast.emit('Plus user',roomId);
        socket.emit('Add roomName to game',roomsKeyId.get(roomId).roomName);
        socket.emit('Create players',roomClients);
        socket.to(roomId).emit('Create players',[userData]);
      }else{
        socket.emit('Error','Maximum number of players in the room');
      }
    });
    socket.on('Leave from room',(roomId,userData)=>{
      removeUserInMap(roomId,userData.username);
      socket.to(roomId).emit('Delete player',userData.userId);
      socket.leave(roomId);
      socket.broadcast.emit('Minus user',roomId);
      roomsKeyId.get(roomId).counter === clients.get(roomId).length && clients.get(roomId).length !== 0
        ? sendToAllClientInRoom(socket,'Start timer',roomId, config.SECONDS_TIMER_BEFORE_START_GAME)
        : null;
      if(results.get(roomId) && clients.get(roomId).length === results.get(roomId).length){
        sendEndGamePar(socket,roomId);
      }
    });
    socket.on('Player Ready',(idPlayer,roomId)=>{
      decremCount(roomId);
      sendToAllClientInRoom(socket,'change ready',roomId,idPlayer);
      if(roomsKeyId.get(roomId).counter){
        if(roomsKeyId.get(roomId).counter === clients.get(roomId).length){
          sendToAllClientInRoom(socket,'Start timer',roomId, config.SECONDS_TIMER_BEFORE_START_GAME);
          socket.broadcast.emit("Hide room",roomId);
        }
      };
    });
    socket.on('Player NotReady',(id,roomId)=>{
      incremCount(roomId);
      sendToAllClientInRoom(socket,'change notReady',roomId,id);
    });
    socket.on('Start Game',(roomId)=>{
      const randomNumber = getRandomTexts(0,6);
      clients.get(roomId).length === 1 
        ?  socket.emit("Start games",{text:texts.texts[randomNumber],time:config.SECONDS_FOR_GAME})
        :  socket.to(roomId).emit("Start games",{text:texts.texts[randomNumber],time:config.SECONDS_FOR_GAME});
    });
    socket.on('Change Line',(roomId,userId,percent)=>{
      sendToAllClientInRoom(socket,'Change player line',roomId,{id:userId,percent});
    });
    socket.on('Player finish',(roomId,userId)=>{
      setResult(roomId,userId,100);
      sendToAllClientInRoom(socket,'Set light-green for line',roomId,userId);
      if(clients.get(roomId).length === results.get(roomId).length){
        sendEndGamePar(socket,roomId);
      }
    });
    socket.on('Game end',(roomId,userId,percent)=>{
      setResult(roomId,userId,percent);
      if(results.get(roomId) && clients.get(roomId).length !== results.get(roomId).length){
        sendEndGamePar(socket,roomId);
      }
    });
    socket.on('Close Window',(roomId, userId, username)=>{
      socket.leave(roomId);
      socket.to(roomId).emit('Delete player',userId);
      clients.get(roomId)
        ? removeUserInMap(roomId,username)
        : null;
      socket.broadcast.emit('Minus user',roomId);
      if(roomsKeyId.get(roomId).counter){
        if(roomsKeyId.get(roomId).counter === clients.get(roomId).length && clients.get(roomId).length !== 0){
        sendToAllClientInRoom(socket,'Start timer',roomId, config.SECONDS_TIMER_BEFORE_START_GAME)
        }
      }
    })
  });
};

function sendEndGamePar(socket,roomId){
  socket.broadcast.emit('Show room',roomId);
  roomsKeyId.get(roomId).counter = 0;
  const dataAboutPlayers = deleteDuplicates(results.get(roomId));
  const winners = getWinnersName(roomId,sortWinners(dataAboutPlayers));

  if(clients.get(roomId).length === winners.length){
    sendToAllClientInRoom(socket,'Winners',roomId,winners),
    sendToAllClientInRoom(socket,'Clear lines',roomId,clients.get(roomId));
  };
  results.delete(roomId);
}

function sendToAllClientInRoom(socket,key,roomId,data){
  socket.emit(key, data);
  socket.to(roomId).emit(key,data);
}

function getRandomTexts(min,max){
  return Math.floor(Math.random() * (max - min) + min);
}